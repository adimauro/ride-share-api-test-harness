﻿using System;
using System.Web;
using Uber.Authentication.Data.Repository;


namespace RideShareTestHarness
{
    public partial class Default : System.Web.UI.Page
    {
        private const string REDIRECT_URL = @"https://localhost:44360/rideshare.aspx";
        private const string CLIENT_ID = "5KXF6BwMtpmnbjeWN78_ZrCNWG4vsIIf";
        private const string CLIENT_SECRET = "7C-1ZaKyxeu_mkeY8haKqmUyuFyHlz_OswTaMbTU";


        private const string UBERHEALTH_REDIRECT_URL = @"https://localhost:44360/uberhealth.aspx";
        private const string UBERHEALTH_CLIENT_ID = "fDO57EXSjxl1pGLS3pcgIQ8HCe30oH7i";
        private const string UBERHEALTH_CLIENT_SECRET = "orcYLk-FoVB0_DCHEhI-SC4AUCgB2jiZTfPASiDT";


        //RideShareService _rideShareService = new RideShareService(new UberRideShareService());

        protected void Page_Load(object sender, EventArgs e)
        {

            //Sample Data

            FirstNameTextBox.Text = "Anthony";
            LastNameTextBox.Text = "DiMauro";
            EmailTextBox.Text = "adimauro@lightbeamhealth.com";
            PhoneNumberTextBox.Text = "6105551234";

            PickUpAddressTextBox.Text = "125 Cedar Avenue";
            PickUpCityTextBox.Text = "Conshohocken";
            PickUpStateDropDown.Text = "PA";
            PickUpZipCodeTextBox.Text = "19428";

            DestinationAddressTextBox.Text = "400 Norristown Road, Suite 107";
            DestinationCityTextBox.Text = "Horsham";
            DestinationStateDropDown.Text = "PA";
            DestinationZipCodeTextBox.Text = "19044";
        }




        protected void RideRequest_Click(object sender, EventArgs e)
        {
            SetPickupDropoff();

            var uber = new UberAuthentication(CLIENT_ID, CLIENT_SECRET);
            var authorizationUrl = uber.GetAuthorizeUrl(null, null, REDIRECT_URL);
            Response.Redirect(authorizationUrl, true);
        }


        protected void HealthRideRequest_Click(object sender, EventArgs e)
        {
            SetPickupDropoff();

            Response.Redirect(UBERHEALTH_REDIRECT_URL, true);
        }






        protected void SetPickupDropoff()
        {
            UberHealthSandboxRun uberHealthSandboxRun;

            if (HttpContext.Current.Cache["UberHealthRecord"] != null)
            {
                uberHealthSandboxRun = (UberHealthSandboxRun)HttpContext.Current.Cache["UberHealthRecord"];
            }
            else
            {
                uberHealthSandboxRun = new UberHealthSandboxRun();
            }

            uberHealthSandboxRun.PatientFirstName = FirstNameTextBox.Text;
            uberHealthSandboxRun.PatientLastName = LastNameTextBox.Text;
            uberHealthSandboxRun.PatientEmail = EmailTextBox.Text;
            uberHealthSandboxRun.PatientPhoneNumber = PhoneNumberTextBox.Text;

            uberHealthSandboxRun.PickupAddress = PickUpAddressTextBox.Text;
            uberHealthSandboxRun.PickupCity = PickUpCityTextBox.Text;
            uberHealthSandboxRun.PickupState = PickUpStateDropDown.Text;
            uberHealthSandboxRun.PickupZipCode = PickUpZipCodeTextBox.Text;

            uberHealthSandboxRun.DropoffAddress = DestinationAddressTextBox.Text;
            uberHealthSandboxRun.DropoffCity = DestinationCityTextBox.Text;
            uberHealthSandboxRun.DropoffState = DestinationStateDropDown.Text;
            uberHealthSandboxRun.DropoffZipCode = DestinationZipCodeTextBox.Text;

            HttpContext.Current.Cache["UberHealthRecord"] = uberHealthSandboxRun;
        }


    }






}

