﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Geocoding.aspx.cs" Inherits="RideShareTestHarness.Geocoding" Async="True"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LB Ride Integration Test App</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<script>
    <a href="packages.config">packages.config</a>
</script>
<body>
<div class="container">

    <form id="form1" runat="server">
        <div class="m-lg-4">
            <br /><br />

            <div class="form-row pt-2">
                <div class="col-md-6">
                    <asp:Label ID="AddressLabel" runat="server" Text="Address*"></asp:Label>
                    <asp:TextBox ID="AddressTextBox" runat="server" class="form-control" oninput="checkSubmitButtonEnable()"></asp:TextBox>
		        </div>
		    </div>

		    <div class="form-row pt-2">
                <div class="col-md-4">
                    <asp:Label ID="CityLabel" runat="server" Text="City*"></asp:Label>
                    <asp:TextBox ID="CityTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
                <div class="col-md-2">
                    <asp:Label ID="StateLabel" runat="server" Text="State*"></asp:Label>
	                <asp:DropDownList ID="StateDropDown" runat="server"  class="form-control" >
	                    <asp:ListItem Value="AL">AL</asp:ListItem>
	                    <asp:ListItem Value="AK">AK</asp:ListItem>
	                    <asp:ListItem Value="AZ">AZ</asp:ListItem>
	                    <asp:ListItem Value="AR">AR</asp:ListItem>
	                    <asp:ListItem Value="CA">CA</asp:ListItem>
	                    <asp:ListItem Value="CO">CO</asp:ListItem>
	                    <asp:ListItem Value="CT">CT</asp:ListItem>
	                    <asp:ListItem Value="DC">DC</asp:ListItem>
	                    <asp:ListItem Value="DE">DE</asp:ListItem>
	                    <asp:ListItem Value="FL">FL</asp:ListItem>
	                    <asp:ListItem Value="GA">GA</asp:ListItem>
	                    <asp:ListItem Value="HI">HI</asp:ListItem>
	                    <asp:ListItem Value="ID">ID</asp:ListItem>
	                    <asp:ListItem Value="IL">IL</asp:ListItem>
	                    <asp:ListItem Value="IN">IN</asp:ListItem>
	                    <asp:ListItem Value="IA">IA</asp:ListItem>
	                    <asp:ListItem Value="KS">KS</asp:ListItem>
	                    <asp:ListItem Value="KY">KY</asp:ListItem>
	                    <asp:ListItem Value="LA">LA</asp:ListItem>
	                    <asp:ListItem Value="ME">ME</asp:ListItem>
	                    <asp:ListItem Value="MD">MD</asp:ListItem>
	                    <asp:ListItem Value="MA">MA</asp:ListItem>
	                    <asp:ListItem Value="MI">MI</asp:ListItem>
	                    <asp:ListItem Value="MN">MN</asp:ListItem>
	                    <asp:ListItem Value="MS">MS</asp:ListItem>
	                    <asp:ListItem Value="MO">MO</asp:ListItem>
	                    <asp:ListItem Value="MT">MT</asp:ListItem>
	                    <asp:ListItem Value="NE">NE</asp:ListItem>
	                    <asp:ListItem Value="NV">NV</asp:ListItem>
	                    <asp:ListItem Value="NH">NH</asp:ListItem>
	                    <asp:ListItem Value="NJ">NJ</asp:ListItem>
	                    <asp:ListItem Value="NM">NM</asp:ListItem>
	                    <asp:ListItem Value="NY">NY</asp:ListItem>
	                    <asp:ListItem Value="NC">NC</asp:ListItem>
	                    <asp:ListItem Value="ND">ND</asp:ListItem>
	                    <asp:ListItem Value="OH">OH</asp:ListItem>
	                    <asp:ListItem Value="OK">OK</asp:ListItem>
	                    <asp:ListItem Value="OR">OR</asp:ListItem>
	                    <asp:ListItem Value="PA">PA</asp:ListItem>
	                    <asp:ListItem Value="RI">RI</asp:ListItem>
	                    <asp:ListItem Value="SC">SC</asp:ListItem>
	                    <asp:ListItem Value="SD">SD</asp:ListItem>
	                    <asp:ListItem Value="TN">TN</asp:ListItem>
	                    <asp:ListItem Value="TX">TX</asp:ListItem>
	                    <asp:ListItem Value="UT">UT</asp:ListItem>
	                    <asp:ListItem Value="VT">VT</asp:ListItem>
	                    <asp:ListItem Value="VA">VA</asp:ListItem>
	                    <asp:ListItem Value="WA">WA</asp:ListItem>
	                    <asp:ListItem Value="WV">WV</asp:ListItem>
	                    <asp:ListItem Value="WI">WI</asp:ListItem>
	                    <asp:ListItem Value="WY">WY</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="ZipCodeLabel" runat="server" Text="Zip*"></asp:Label>
                    <asp:TextBox ID="ZipCodeTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
		    </div>
		    <div class="form-row pt-2">
				<asp:Button ID="ForwardGeocodeButton" runat="server" class="btn btn-primary" Text="Get Coordinates From Address" OnClick="ForwardGeocodeButton_Click" />
		    &nbsp;&nbsp;
				<asp:Button ID="GetZipCodeButton" runat="server" class="btn btn-primary" Text="Get ZipCode For Address" OnClick="GetZipCodeButton_Click" />
		    </div>
			<div class="form-row pt-4">
                <div class="col-md-3">
                    <asp:Label ID="LatitudeLabel" runat="server" Text="Latitude"></asp:Label>
                    <asp:TextBox ID="LatitudeTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
                <div class="col-md-3">
                    <asp:Label ID="LongitudeLabel" runat="server" Text="Longitude"></asp:Label>
                    <asp:TextBox ID="LongitudeTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
		    </div>
		    <div class="form-row pt-2">
				<asp:Button ID="ReverseGeocodeButton" runat="server" class="btn btn-primary" Text="Get Address From Coordinates" OnClick="ReverseGeocodeButton_Click" />
		    </div>

        </div>
    </form>
</div>
</body>
</html>
