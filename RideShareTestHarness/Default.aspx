﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RideShareTestHarness.Default"   %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LB Ride Integration Test App</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">

    </style>
</head>



<script>


    function ShowLogin() {

        //Document Viewer is in its own webpage, load it in an iFrame of the Pop-up
        //This implementation prevents post-back on the page.
        document.getElementById("LoginFrame").src = "Login.aspx";

        $("#popupLogin .modal-title").html("Ride Share Login");
        $("#popupLogin").modal("show");
    }
    //END - Show Document Viewer Modal Popup

</script>  


<body>
<div class="container">
    <form id="form1" runat="server">
        <div class="col-lg-12   pt-4">
			<h5 style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; line-height: 1.25; font-family: ff-clan-web-pro, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-weight: 400; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Patient</h5>


            <div class="form-row">
                <div class="col-md-3">
                    <asp:Label ID="FirstNameLabel" runat="server" Text="First Name"></asp:Label>
                    <asp:TextBox ID="FirstNameTextBox" runat="server" class="form-control" oninput="checkSubmitButtonEnable()"></asp:TextBox>
		        </div>
                <div class="col-md-3">
                    <asp:Label ID="LastNameLabel" runat="server" Text="Last Name*"></asp:Label>
                    <asp:TextBox ID="LastNameTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
		    </div>
			<div class="form-row pt-2">
                <div class="col-md-6">
                    <asp:Label ID="PhoneNumberLabel" runat="server" Text="Phone Number"></asp:Label>
                    <asp:TextBox ID="PhoneNumberTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
		    </div>

		    <div class="form-row pt-2">
                <div class="col-md-6">
                    <asp:Label ID="EmailLabel" runat="server" Text="Email Address"></asp:Label>
                    <asp:TextBox ID="EmailTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
		    </div>
        </div>


        <div class="col-md-12   pt-4">
			<h5 style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; line-height: 1.25; font-family: ff-clan-web-pro, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-weight: 400; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Pick Up Location</h5>


            <div class="form-row">
                <div class="col-md-6">
                    <asp:Label ID="PickUpAddressLabel" runat="server" Text="Address*"></asp:Label>
                    <asp:TextBox ID="PickUpAddressTextBox" runat="server" class="form-control" oninput="checkSubmitButtonEnable()"></asp:TextBox>
		        </div>
		    </div>

		    <div class="form-row pt-2">
                <div class="col-md-4">
                    <asp:Label ID="PickUpCityLabel" runat="server" Text="City*"></asp:Label>
                    <asp:TextBox ID="PickUpCityTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
                <div class="col-md-2">
                    <asp:Label ID="PickUpStateLabel" runat="server" Text="State*"></asp:Label>
	                <asp:DropDownList ID="PickUpStateDropDown" runat="server"  class="form-control" >
	                    <asp:ListItem Value="AL">AL</asp:ListItem>
	                    <asp:ListItem Value="AK">AK</asp:ListItem>
	                    <asp:ListItem Value="AZ">AZ</asp:ListItem>
	                    <asp:ListItem Value="AR">AR</asp:ListItem>
	                    <asp:ListItem Value="CA">CA</asp:ListItem>
	                    <asp:ListItem Value="CO">CO</asp:ListItem>
	                    <asp:ListItem Value="CT">CT</asp:ListItem>
	                    <asp:ListItem Value="DC">DC</asp:ListItem>
	                    <asp:ListItem Value="DE">DE</asp:ListItem>
	                    <asp:ListItem Value="FL">FL</asp:ListItem>
	                    <asp:ListItem Value="GA">GA</asp:ListItem>
	                    <asp:ListItem Value="HI">HI</asp:ListItem>
	                    <asp:ListItem Value="ID">ID</asp:ListItem>
	                    <asp:ListItem Value="IL">IL</asp:ListItem>
	                    <asp:ListItem Value="IN">IN</asp:ListItem>
	                    <asp:ListItem Value="IA">IA</asp:ListItem>
	                    <asp:ListItem Value="KS">KS</asp:ListItem>
	                    <asp:ListItem Value="KY">KY</asp:ListItem>
	                    <asp:ListItem Value="LA">LA</asp:ListItem>
	                    <asp:ListItem Value="ME">ME</asp:ListItem>
	                    <asp:ListItem Value="MD">MD</asp:ListItem>
	                    <asp:ListItem Value="MA">MA</asp:ListItem>
	                    <asp:ListItem Value="MI">MI</asp:ListItem>
	                    <asp:ListItem Value="MN">MN</asp:ListItem>
	                    <asp:ListItem Value="MS">MS</asp:ListItem>
	                    <asp:ListItem Value="MO">MO</asp:ListItem>
	                    <asp:ListItem Value="MT">MT</asp:ListItem>
	                    <asp:ListItem Value="NE">NE</asp:ListItem>
	                    <asp:ListItem Value="NV">NV</asp:ListItem>
	                    <asp:ListItem Value="NH">NH</asp:ListItem>
	                    <asp:ListItem Value="NJ">NJ</asp:ListItem>
	                    <asp:ListItem Value="NM">NM</asp:ListItem>
	                    <asp:ListItem Value="NY">NY</asp:ListItem>
	                    <asp:ListItem Value="NC">NC</asp:ListItem>
	                    <asp:ListItem Value="ND">ND</asp:ListItem>
	                    <asp:ListItem Value="OH">OH</asp:ListItem>
	                    <asp:ListItem Value="OK">OK</asp:ListItem>
	                    <asp:ListItem Value="OR">OR</asp:ListItem>
	                    <asp:ListItem Value="PA">PA</asp:ListItem>
	                    <asp:ListItem Value="RI">RI</asp:ListItem>
	                    <asp:ListItem Value="SC">SC</asp:ListItem>
	                    <asp:ListItem Value="SD">SD</asp:ListItem>
	                    <asp:ListItem Value="TN">TN</asp:ListItem>
	                    <asp:ListItem Value="TX">TX</asp:ListItem>
	                    <asp:ListItem Value="UT">UT</asp:ListItem>
	                    <asp:ListItem Value="VT">VT</asp:ListItem>
	                    <asp:ListItem Value="VA">VA</asp:ListItem>
	                    <asp:ListItem Value="WA">WA</asp:ListItem>
	                    <asp:ListItem Value="WV">WV</asp:ListItem>
	                    <asp:ListItem Value="WI">WI</asp:ListItem>
	                    <asp:ListItem Value="WY">WY</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="PickUpZipCodeLabel" runat="server" Text="Zip*"></asp:Label>
                    <asp:TextBox ID="PickUpZipCodeTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
		    </div>
        </div>


        <div class="col-md-12   pt-4">
			<h5 style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; line-height: 1.25; font-family: ff-clan-web-pro, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-weight: 400; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Destination</h5>
            <div class="form-row">
                <div class="col-md-6">
                    <asp:Label ID="DestinationAddressLabel" runat="server" Text="Address*"></asp:Label>
                    <asp:TextBox ID="DestinationAddressTextBox" runat="server" class="form-control" oninput="checkSubmitButtonEnable()"></asp:TextBox>
		        </div>
		    </div>

		    <div class="form-row pt-2">
                <div class="col-md-4">
                    <asp:Label ID="DestinationCityLabel" runat="server" Text="City*"></asp:Label>
                    <asp:TextBox ID="DestinationCityTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
                <div class="col-md-2">
                    <asp:Label ID="DestinationStateLabel" runat="server" Text="State*"></asp:Label>
	                <asp:DropDownList ID="DestinationStateDropDown" runat="server"  class="form-control" >
	                    <asp:ListItem Value="AL">AL</asp:ListItem>
	                    <asp:ListItem Value="AK">AK</asp:ListItem>
	                    <asp:ListItem Value="AZ">AZ</asp:ListItem>
	                    <asp:ListItem Value="AR">AR</asp:ListItem>
	                    <asp:ListItem Value="CA">CA</asp:ListItem>
	                    <asp:ListItem Value="CO">CO</asp:ListItem>
	                    <asp:ListItem Value="CT">CT</asp:ListItem>
	                    <asp:ListItem Value="DC">DC</asp:ListItem>
	                    <asp:ListItem Value="DE">DE</asp:ListItem>
	                    <asp:ListItem Value="FL">FL</asp:ListItem>
	                    <asp:ListItem Value="GA">GA</asp:ListItem>
	                    <asp:ListItem Value="HI">HI</asp:ListItem>
	                    <asp:ListItem Value="ID">ID</asp:ListItem>
	                    <asp:ListItem Value="IL">IL</asp:ListItem>
	                    <asp:ListItem Value="IN">IN</asp:ListItem>
	                    <asp:ListItem Value="IA">IA</asp:ListItem>
	                    <asp:ListItem Value="KS">KS</asp:ListItem>
	                    <asp:ListItem Value="KY">KY</asp:ListItem>
	                    <asp:ListItem Value="LA">LA</asp:ListItem>
	                    <asp:ListItem Value="ME">ME</asp:ListItem>
	                    <asp:ListItem Value="MD">MD</asp:ListItem>
	                    <asp:ListItem Value="MA">MA</asp:ListItem>
	                    <asp:ListItem Value="MI">MI</asp:ListItem>
	                    <asp:ListItem Value="MN">MN</asp:ListItem>
	                    <asp:ListItem Value="MS">MS</asp:ListItem>
	                    <asp:ListItem Value="MO">MO</asp:ListItem>
	                    <asp:ListItem Value="MT">MT</asp:ListItem>
	                    <asp:ListItem Value="NE">NE</asp:ListItem>
	                    <asp:ListItem Value="NV">NV</asp:ListItem>
	                    <asp:ListItem Value="NH">NH</asp:ListItem>
	                    <asp:ListItem Value="NJ">NJ</asp:ListItem>
	                    <asp:ListItem Value="NM">NM</asp:ListItem>
	                    <asp:ListItem Value="NY">NY</asp:ListItem>
	                    <asp:ListItem Value="NC">NC</asp:ListItem>
	                    <asp:ListItem Value="ND">ND</asp:ListItem>
	                    <asp:ListItem Value="OH">OH</asp:ListItem>
	                    <asp:ListItem Value="OK">OK</asp:ListItem>
	                    <asp:ListItem Value="OR">OR</asp:ListItem>
	                    <asp:ListItem Value="PA">PA</asp:ListItem>
	                    <asp:ListItem Value="RI">RI</asp:ListItem>
	                    <asp:ListItem Value="SC">SC</asp:ListItem>
	                    <asp:ListItem Value="SD">SD</asp:ListItem>
	                    <asp:ListItem Value="TN">TN</asp:ListItem>
	                    <asp:ListItem Value="TX">TX</asp:ListItem>
	                    <asp:ListItem Value="UT">UT</asp:ListItem>
	                    <asp:ListItem Value="VT">VT</asp:ListItem>
	                    <asp:ListItem Value="VA">VA</asp:ListItem>
	                    <asp:ListItem Value="WA">WA</asp:ListItem>
	                    <asp:ListItem Value="WV">WV</asp:ListItem>
	                    <asp:ListItem Value="WI">WI</asp:ListItem>
	                    <asp:ListItem Value="WY">WY</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="DestinationZipCodeLabel" runat="server" Text="Zip*"></asp:Label>
                    <asp:TextBox ID="DestinationZipCodeTextBox" runat="server" class="form-control"></asp:TextBox>
	            </div>
		    </div>
        </div>

        <div class="col-md-12 pt-4">
			<asp:Button ID="RequestUberRideButton" runat="server" class="btn btn-primary" Text="Request Uber Ride" OnClick="HealthRideRequest_Click" />
        </div>

    </form>
</div>


<!-- Modal Popup -->
<div id="popupLogin" class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" style="width:75%" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body h-100">

                <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                  <iframe id="LoginFrame" class="embed-responsive-item" src="" scrolling="no"  allowfullscreen></iframe>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>



</body>
</html>
