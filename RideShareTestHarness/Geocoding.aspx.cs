﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Net;
using System.Net.Http;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Uber.Rider.Data.Models;

namespace RideShareTestHarness
{
    public partial class Geocoding : System.Web.UI.Page
    {

        //GeocodeService _geocodeService = new GeocodeService(new GoogleGeocodeService());
        GeocodeService _geocodeService = new GeocodeService(new MapquestGeocodeService());




        protected void Page_Load(object sender, EventArgs e)
        {

            string stateAbbr = States.AK.ToString();
            string state = States.DC.ToDescription();
            var enumState = "PR".ToEnum<States>();
            var stateAbbr2 = ("ALABAMA").ToEnumFromDescription<States>();

            var stateList = EnumExtensions.ToList<States>();


            //ListItem item = new ListItem();
            //StateDropDown.Items.Add(item);
        }




        protected void ForwardGeocodeButton_Click(object sender, EventArgs e)
        {

            LocationRecord location = new LocationRecord(AddressTextBox.Text, 
                                                            CityTextBox.Text, 
                                                            StateDropDown.Text, 
                                                            ZipCodeTextBox.Text);

            location = _geocodeService.GetCoordinates(location);

            LatitudeTextBox.Text = location.Latitude.ToString();
            LongitudeTextBox.Text = location.Longitude.ToString(); ;
        }


        protected void ReverseGeocodeButton_Click(object sender, EventArgs e)
        {
            LocationRecord location = _geocodeService.GetAddress(Convert.ToDouble(LatitudeTextBox.Text), Convert.ToDouble(LongitudeTextBox.Text));

            AddressTextBox.Text = location.AddressLine1;
            CityTextBox.Text = location.City;
            StateDropDown.Text = location.State;
            ZipCodeTextBox.Text = location.ZipCode;

        }

        protected void GetZipCodeButton_Click(object sender, EventArgs e)
        {

            LocationRecord location = new LocationRecord(AddressTextBox.Text,
                                                            CityTextBox.Text,
                                                            StateDropDown.Text,
                                                            string.Empty);


            ZipCodeTextBox.Text = _geocodeService.GetZipCodeForAddress(location);
        }
    }



}