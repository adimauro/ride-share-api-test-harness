﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"  CodeBehind="RideShare.aspx.cs" Inherits="RideShareTestHarness.RideShare"   %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LB Ride Integration Test App</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">


    </style>
</head>

<script>
</script>
<body>
<div class="container">

    <form id="form1" runat="server">
        <div class="m-lg-4">
            <br />
            <h2 id="user-profile0"  style="box-sizing: border-box; font-variant-ligatures: normal; font-variant-caps: normal; orphans: 2; widows: 2; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">Authentication</h2>
            <div class="form-row">
                <div class="col-md-12">
                    <asp:Label ID="AuthorizationCodeLabel" runat="server" Text="Authorization Code"></asp:Label>
                    <asp:TextBox ID="AuthorizationCodeTextBox" runat="server" class="form-control"></asp:TextBox>
		        </div>
		    </div>
            
            <div class="form-row pt-2">
                <div class="col-md-12">
                    <asp:Label ID="AccessTokenLabel" runat="server" Text="Access Token"></asp:Label>
                    <asp:TextBox ID="AccessTokenTextBox" runat="server" class="form-control"></asp:TextBox>
                    <asp:Button ID="AccessTokenButton" runat="server" class="btn btn-secondary" Text="Get Access Token" OnClick="GetAccessTokenButton_Click" />
		        </div>
		    </div>

            <br /><br />
            <h2 style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; line-height: 1.25; font-family: ff-clan-web-pro, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-weight: 400; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Riders</h2>
            
            <h5 style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; line-height: 1.25; font-family: ff-clan-web-pro, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-weight: 400; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">User Profile</h5>
            <asp:TextBox ID="UserProfileTextBox" runat="server" class="form-control" Height="213px" TextMode="MultiLine" Width="972px" Rows="4"></asp:TextBox>
            <asp:Button ID="GetProfileButton" runat="server" class="btn btn-secondary" Text="Get User Profile" OnClick="GetUserProfileButton_Click" />
            <br /><br />


            <h5 style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; line-height: 1.25; font-family: ff-clan-web-pro, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-weight: 400; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">User Activity</h5>
            <asp:TextBox ID="UserActivityTextBox" runat="server" class="form-control" Height="213px" TextMode="MultiLine" Width="972px" Rows="4"></asp:TextBox>
            <asp:Button ID="GetUserActivityButton" runat="server" class="btn btn-secondary" Text="Get User Activity" OnClick="GetUserActivityButton_Click" />
            <br /><br />

            <h2 style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; line-height: 1.25; font-family: ff-clan-web-pro, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-weight: 400; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Ride Products</h2>

            <h5 style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; line-height: 1.25; font-family: ff-clan-web-pro, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-weight: 400; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Products</h5>
            <asp:TextBox ID="ProductsTextBox" runat="server" class="form-control" Height="213px" TextMode="MultiLine" Width="972px" Rows="4"></asp:TextBox>
            <asp:Button ID="GetProductsButton" runat="server" class="btn btn-secondary" Text="Get Products" OnClick="GetProductsButton_Click" />
            <br /><br /><br /><br />



        </div>
    </form>
</div>
</body>
</html>
