﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Uber.Authentication.Data.Models;
using Uber.Health.Data.Models;

namespace RideShareTestHarness
{
    public class UberHealthSandboxRun
    {
        public static string CLIENT_ID = ConfigurationManager.AppSettings["Uber:ClientId"];
        public static string CLIENT_SECRET = ConfigurationManager.AppSettings["Uber:ClientSecret"];
        private static string REDIRECT_URL = ConfigurationManager.AppSettings["Uber:RedirectUri"];
        private static string SCOPES = ConfigurationManager.AppSettings["Uber:Scopes"];
        private static string WEBHOOK_URL = ConfigurationManager.AppSettings["Uber:WebhookUri"];

        public UberHealthSandboxRun()
        {

        }

        public  AccessToken AccessToken { get; set; }
        public string RunId { get; set; }
        public string DriverId { get; set; }

        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public string PatientEmail { get; set; }
        public string PatientPhoneNumber { get; set; }


        public string PickupAddress { get; set; }
        public string PickupCity { get; set; }
        public string PickupState { get; set; }
        public string PickupZipCode { get; set; }


        public string DropoffAddress { get; set; }
        public string DropoffCity { get; set; }
        public string DropoffState { get; set; }
        public string DropoffZipCode { get; set; }

        public List<ProductEstimate> ProductEstimates { get; set; }

    }
}