﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Uber.Health.Data.Models;
using Uber.Health.Data.Repository;
using Uber.Authentication.Data.Repository;
using System.Threading.Tasks;
using System.Web.UI;

namespace RideShareTestHarness
{
    public partial class UberHealth : System.Web.UI.Page
    {

        //Settings that would need to be configurable in the Portal Management Portal
        //Uber API version should probably be a setting as well.
        private const string CLIENT_ID = "fDO57EXSjxl1pGLS3pcgIQ8HCe30oH7i";
        private const string CLIENT_SECRET = "orcYLk-FoVB0_DCHEhI-SC4AUCgB2jiZTfPASiDT";
        private const string SCOPES = "health.sandbox";
        private const string REDIRECT_URL = @"https://localhost:44360/rideshare.aspx";
        private const string WEBHOOK_URL = @"https://localhost:44360/webhook.aspx";




        GeocodeService _geocodeService = new GeocodeService(new MapquestGeocodeService());

        UberHealthSandboxRun _uberHealthSandboxRun;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TripRequestButton.Visible = false;
            }

            if (HttpContext.Current.Cache["UberHealthRecord"] != null)
            {
                _uberHealthSandboxRun = (UberHealthSandboxRun)HttpContext.Current.Cache["UberHealthRecord"];

                if (_uberHealthSandboxRun.AccessToken == null)
                {
                    //GetAccessToken();
                    RegisterAsyncTask(new PageAsyncTask(GetAccessTokenAsync));
                }

                if (_uberHealthSandboxRun.ProductEstimates != null)
                {
                    LoadProductEstimates(_uberHealthSandboxRun.ProductEstimates);
                    TripRequestButton.Visible = true;
                }
            }
            else
            {
                _uberHealthSandboxRun = new UberHealthSandboxRun();
                AccessTokenTextBox.Text = _uberHealthSandboxRun.AccessToken.Value;
            }

            SandboxRideTypeDropDown.Items.Clear();
            var uberRideTypeList = EnumExtensions.ToList<UberRideType>();
            foreach (var uberRideType in uberRideTypeList)
            {
                SandboxRideTypeDropDown.Items.Add(uberRideType.ToString());
            }

            SandboxDriverStateDropDown.Items.Clear();
            var uberDriverStateList = EnumExtensions.ToList<UberDriverState>();
            foreach (var uberDriverState in uberDriverStateList)
            {
                SandboxDriverStateDropDown.Items.Add(uberDriverState.ToString());
            }
        }



        protected void CreateSandboxRunButton_Click(object sender, EventArgs e)
        {
            //CreateSandboxRun();
            RegisterAsyncTask(new PageAsyncTask(CreateSandboxRunAsync));

        }

        protected void GetSandboxDriverButton_Click(object sender, EventArgs e)
        {
            //GetSandboxDriver();
            RegisterAsyncTask(new PageAsyncTask(GetSandboxDriverAsync));
        }

        protected void GetProductsButton_Click(object sender, EventArgs e)
        {
            //GetProducts();
            RegisterAsyncTask(new PageAsyncTask(GetProductsAsync));
        }

        protected void SetSandboxDriverStateButton_Click(object sender, EventArgs e)
        {

            //SetSandboxDriverState();
            RegisterAsyncTask(new PageAsyncTask(SetSandboxDriverStateAsync));
        }

        protected void TripRequestButton_Click(object sender, EventArgs e)
        {

            //TripRequest();
            RegisterAsyncTask(new PageAsyncTask(TripRequestAsync));
        }




        #region [Uber Non-Async Methods]

        private void GetAccessToken()
        {
            var uber = new UberAuthentication(CLIENT_ID, CLIENT_SECRET);

            var scopesSettings = SCOPES.Split(',');

            var scopes = new List<string>();
            for (int ix = 0; ix < scopesSettings.Length; ix++)
            {
                scopes.Add(scopesSettings[ix].Trim());
            }

            var authorizationCode = uber.GetAuthorization(scopes, null, REDIRECT_URL);

            var accessToken = uber.GetAccessToken(scopes, REDIRECT_URL);
            AccessTokenTextBox.Text = accessToken.Value;

            _uberHealthSandboxRun.AccessToken = accessToken;
        }



        private void CreateSandboxRun()
        {
            var uberSandbox = new SandboxAuthenticatedUberHealthService(_uberHealthSandboxRun.AccessToken.Value);

            var pickupLocation = new LocationRecord(_uberHealthSandboxRun.PickupAddress, _uberHealthSandboxRun.PickupCity, _uberHealthSandboxRun.PickupState, _uberHealthSandboxRun.PickupZipCode);
            pickupLocation = _geocodeService.GetCoordinates(pickupLocation);

            var dropoffLocation = new LocationRecord(_uberHealthSandboxRun.DropoffAddress, _uberHealthSandboxRun.DropoffCity, _uberHealthSandboxRun.DropoffState, _uberHealthSandboxRun.DropoffZipCode);
            dropoffLocation = _geocodeService.GetCoordinates(dropoffLocation);

            var uberRideType = SandboxRideTypeDropDown.SelectedValue.ToEnum<UberRideType>();

            var sandboxRun = uberSandbox.GetRunRequest(uberRideType, pickupLocation.Latitude, pickupLocation.Longitude, dropoffLocation.Latitude, dropoffLocation.Longitude, null, null);
            _uberHealthSandboxRun.RunId = sandboxRun.Data.RunId;
            SandboxRunIdTextBox.Text = _uberHealthSandboxRun.RunId;
        }


        private void GetSandboxDriver()
        {
            string driverData = string.Empty;

            var uberSandbox = new SandboxAuthenticatedUberHealthService(_uberHealthSandboxRun.AccessToken.Value);
            var sandboxDriver = uberSandbox.GetRunDriver(SandboxRunIdTextBox.Text);

            if (sandboxDriver.Data == null)
            {
                SandboxDriverTextBox.Text = sandboxDriver.Error.Message;
            }
            else
            {
                if (sandboxDriver.Data.DriverId != null)
                {
                    _uberHealthSandboxRun.DriverId = sandboxDriver.Data.DriverId[0];
                    SandboxDriverTextBox.Text = sandboxDriver.Data.DriverId[0];
                }
            }
        }


        private void GetProducts()
        {
            var accessToken = AccessTokenTextBox.Text;
            var runId = SandboxRunIdTextBox.Text;

            var pickupLocation = new LocationRecord(_uberHealthSandboxRun.PickupAddress, _uberHealthSandboxRun.PickupCity, _uberHealthSandboxRun.PickupState, _uberHealthSandboxRun.PickupZipCode);
            pickupLocation = _geocodeService.GetCoordinates(pickupLocation);

            var dropoffLocation = new LocationRecord(_uberHealthSandboxRun.DropoffAddress, _uberHealthSandboxRun.DropoffCity, _uberHealthSandboxRun.DropoffState, _uberHealthSandboxRun.DropoffZipCode);
            dropoffLocation = _geocodeService.GetCoordinates(dropoffLocation);

            var uberHealth = new ClientAuthenticatedUberHealthService(accessToken, runId);
            var uberProductEstimates = uberHealth.GetTripEstimates(pickupLocation.Latitude, pickupLocation.Longitude, dropoffLocation.Latitude, dropoffLocation.Longitude);
            if (uberProductEstimates.Data != null)
            {
                LoadProductEstimates(uberProductEstimates.Data.ProductEstimates);
                _uberHealthSandboxRun.ProductEstimates = uberProductEstimates.Data.ProductEstimates;

                TripRequestButton.Visible = true;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }





        private void TripRequest()
        {
            string productId = SelectedProductHiddenField.Value.ToString();
            var accessToken = AccessTokenTextBox.Text;
            var runId = SandboxRunIdTextBox.Text;

            var guest = new Guest();
            guest.FirstName = _uberHealthSandboxRun.PatientFirstName;
            guest.LastName = _uberHealthSandboxRun.PatientLastName;
            guest.Email = _uberHealthSandboxRun.PatientEmail;
            guest.PhoneNumber = _uberHealthSandboxRun.PatientPhoneNumber;

            var pickupCoordinates = _geocodeService.GetCoordinates(_uberHealthSandboxRun.PickupAddress, _uberHealthSandboxRun.PickupCity, _uberHealthSandboxRun.PickupState, _uberHealthSandboxRun.PickupZipCode);
            Location pickupLocation = new Location();
            pickupLocation.Latitude = pickupCoordinates.Latitude;
            pickupLocation.Longitude = pickupCoordinates.Longitude;
            pickupLocation.Address = _uberHealthSandboxRun.PickupAddress + " " + _uberHealthSandboxRun.PickupCity + " " + _uberHealthSandboxRun.PickupState + " " + _uberHealthSandboxRun.PickupZipCode;

            var dropoffCoordinates = _geocodeService.GetCoordinates(_uberHealthSandboxRun.DropoffAddress, _uberHealthSandboxRun.DropoffCity, _uberHealthSandboxRun.DropoffState, _uberHealthSandboxRun.DropoffZipCode);
            Location dropoffLocation = new Location();
            dropoffLocation.Latitude = dropoffCoordinates.Latitude;
            dropoffLocation.Longitude = dropoffCoordinates.Longitude;
            dropoffLocation.Address = _uberHealthSandboxRun.DropoffAddress + " " + _uberHealthSandboxRun.DropoffCity + " " + _uberHealthSandboxRun.DropoffState + " " + _uberHealthSandboxRun.DropoffZipCode;


            var uberHealth = new ClientAuthenticatedUberHealthService(accessToken, runId);
            var uberTripRequest = uberHealth.GetTripRequest(productId, "Lightbeam Health", guest, pickupLocation, dropoffLocation);

            if (uberTripRequest.Data != null)
            {
                TripResponseTextBox.Text = "Trip Id: " + (string.IsNullOrEmpty(uberTripRequest.Data.TripId) ? string.Empty : uberTripRequest.Data.TripId) + Environment.NewLine +
                                           "Trip Status: " + (string.IsNullOrEmpty(uberTripRequest.Data.Status) ? string.Empty : uberTripRequest.Data.Status) + Environment.NewLine +
                                           "ETA (in Minutes): " + (string.IsNullOrEmpty(uberTripRequest.Data.Eta.ToString()) ? string.Empty : uberTripRequest.Data.Eta.ToString()) + Environment.NewLine; ;

            }
            else if (uberTripRequest.Error != null)
            {
                TripResponseTextBox.Text = "Code: " + (string.IsNullOrEmpty(uberTripRequest.Error.Code) ? string.Empty : uberTripRequest.Error.Code) + Environment.NewLine +
                                          "Message: " + (string.IsNullOrEmpty(uberTripRequest.Error.Message) ? string.Empty : uberTripRequest.Error.Message);

            }


        }



        private void SetSandboxDriverState()
        {


        }
        #endregion



        #region [Uber Async Methods]

        private async Task GetAccessTokenAsync()
        {
            var uber = new UberAuthenticationAsync(CLIENT_ID, CLIENT_SECRET);

            var scopesSettings = SCOPES.Split(',');

            var scopes = new List<string>();
            for (int ix = 0; ix < scopesSettings.Length; ix++)
            {
                scopes.Add(scopesSettings[ix].Trim());
            }

            var accessTokenTask = uber.GetAccessTokenAsync(scopes, REDIRECT_URL);
            var accessToken = await accessTokenTask;

            _uberHealthSandboxRun.AccessToken = accessToken;
            AccessTokenTextBox.Text = accessToken.Value;
        }


        private async Task CreateSandboxRunAsync()
        {
            var uberSandbox = new SandboxAuthenticatedUberHealthServiceAsync(_uberHealthSandboxRun.AccessToken.Value);

            var pickupLocation = new LocationRecord(_uberHealthSandboxRun.PickupAddress, _uberHealthSandboxRun.PickupCity, _uberHealthSandboxRun.PickupState, _uberHealthSandboxRun.PickupZipCode);
            pickupLocation = _geocodeService.GetCoordinates(pickupLocation);

            var dropoffLocation = new LocationRecord(_uberHealthSandboxRun.DropoffAddress, _uberHealthSandboxRun.DropoffCity, _uberHealthSandboxRun.DropoffState, _uberHealthSandboxRun.DropoffZipCode);
            dropoffLocation = _geocodeService.GetCoordinates(dropoffLocation);

            var uberRideType = SandboxRideTypeDropDown.SelectedValue.ToEnum<UberRideType>();

            var sandboxRunTask = uberSandbox.GetRunRequestAsync(uberRideType, pickupLocation.Latitude, pickupLocation.Longitude, dropoffLocation.Latitude, dropoffLocation.Longitude, null, null);
            var sandboxRun = await sandboxRunTask;

            _uberHealthSandboxRun.RunId = sandboxRun.Data.RunId;
            SandboxRunIdTextBox.Text = _uberHealthSandboxRun.RunId;
        }






        private async Task GetSandboxDriverAsync()
        {
            string driverData = string.Empty;

            var uberSandbox = new SandboxAuthenticatedUberHealthServiceAsync(_uberHealthSandboxRun.AccessToken.Value);
            var sandboxDriverTask = uberSandbox.GetRunDriverAsync(SandboxRunIdTextBox.Text);
            var sandboxDriver = await sandboxDriverTask;

            if (sandboxDriver.Data == null)
            {
                SandboxDriverTextBox.Text = sandboxDriver.Error.Message;
            }
            else
            {
                if (sandboxDriver.Data.DriverId != null)
                {
                    _uberHealthSandboxRun.DriverId = sandboxDriver.Data.DriverId[0];
                    SandboxDriverTextBox.Text = sandboxDriver.Data.DriverId[0];
                }
            }
        }





        private async Task GetProductsAsync()
        {
            var accessToken = AccessTokenTextBox.Text;
            var runId = SandboxRunIdTextBox.Text;

            var pickupLocation = new LocationRecord(_uberHealthSandboxRun.PickupAddress, _uberHealthSandboxRun.PickupCity, _uberHealthSandboxRun.PickupState, _uberHealthSandboxRun.PickupZipCode);
            pickupLocation = _geocodeService.GetCoordinates(pickupLocation);

            var dropoffLocation = new LocationRecord(_uberHealthSandboxRun.DropoffAddress, _uberHealthSandboxRun.DropoffCity, _uberHealthSandboxRun.DropoffState, _uberHealthSandboxRun.DropoffZipCode);
            dropoffLocation = _geocodeService.GetCoordinates(dropoffLocation);

            var uberHealth = new ClientAuthenticatedUberHealthServiceAsync(accessToken, runId);
            var uberProductEstimatesTask = uberHealth.GetTripEstimatesAsync(pickupLocation.Latitude, pickupLocation.Longitude, dropoffLocation.Latitude, dropoffLocation.Longitude);
            var uberProductEstimates = await uberProductEstimatesTask;

            if (uberProductEstimates.Data != null)
            {
                LoadProductEstimates(uberProductEstimates.Data.ProductEstimates);
                _uberHealthSandboxRun.ProductEstimates = uberProductEstimates.Data.ProductEstimates;

                TripRequestButton.Visible = true;
            }
        }



        private async Task TripRequestAsync()
        {
            string productId = SelectedProductHiddenField.Value.ToString();
            var accessToken = AccessTokenTextBox.Text;
            var runId = SandboxRunIdTextBox.Text;

            var guest = new Guest();
            guest.FirstName = _uberHealthSandboxRun.PatientFirstName;
            guest.LastName = _uberHealthSandboxRun.PatientLastName;
            guest.Email = _uberHealthSandboxRun.PatientEmail;
            guest.PhoneNumber = _uberHealthSandboxRun.PatientPhoneNumber;

            var pickupCoordinates = _geocodeService.GetCoordinates(_uberHealthSandboxRun.PickupAddress, _uberHealthSandboxRun.PickupCity, _uberHealthSandboxRun.PickupState, _uberHealthSandboxRun.PickupZipCode);
            Location pickupLocation = new Location();
            pickupLocation.Latitude = pickupCoordinates.Latitude;
            pickupLocation.Longitude = pickupCoordinates.Longitude;
            pickupLocation.Address = _uberHealthSandboxRun.PickupAddress + " " + _uberHealthSandboxRun.PickupCity + " " + _uberHealthSandboxRun.PickupState + " " + _uberHealthSandboxRun.PickupZipCode;

            var dropoffCoordinates = _geocodeService.GetCoordinates(_uberHealthSandboxRun.DropoffAddress, _uberHealthSandboxRun.DropoffCity, _uberHealthSandboxRun.DropoffState, _uberHealthSandboxRun.DropoffZipCode);
            Location dropoffLocation = new Location();
            dropoffLocation.Latitude = dropoffCoordinates.Latitude;
            dropoffLocation.Longitude = dropoffCoordinates.Longitude;
            dropoffLocation.Address = _uberHealthSandboxRun.DropoffAddress + " " + _uberHealthSandboxRun.DropoffCity + " " + _uberHealthSandboxRun.DropoffState + " " + _uberHealthSandboxRun.DropoffZipCode;


            var uberHealth = new ClientAuthenticatedUberHealthServiceAsync(accessToken, runId);
            var uberTripRequestTask = uberHealth.GetTripRequestAsync(productId, "Lightbeam Health", guest, pickupLocation, dropoffLocation);
            var uberTripRequest = await uberTripRequestTask;

            if (uberTripRequest.Data != null)
            {
                TripResponseTextBox.Text = "Trip Id: " + (string.IsNullOrEmpty(uberTripRequest.Data.TripId) ? string.Empty : uberTripRequest.Data.TripId) + Environment.NewLine +
                                           "Trip Status: " + (string.IsNullOrEmpty(uberTripRequest.Data.Status) ? string.Empty : uberTripRequest.Data.Status) + Environment.NewLine +
                                           "ETA (in Minutes): " + (string.IsNullOrEmpty(uberTripRequest.Data.Eta.ToString()) ? string.Empty : uberTripRequest.Data.Eta.ToString()) + Environment.NewLine; ;

            }
            else if (uberTripRequest.Error != null)
            {
                TripResponseTextBox.Text = "Code: " + (string.IsNullOrEmpty(uberTripRequest.Error.Code) ? string.Empty : uberTripRequest.Error.Code) + Environment.NewLine +
                                          "Message: " + (string.IsNullOrEmpty(uberTripRequest.Error.Message) ? string.Empty : uberTripRequest.Error.Message);

            }


        }



        private async Task SetSandboxDriverStateAsync()
        {


        }


        #endregion




        #region [Page Control Helper Functions]


        private void LoadProductEstimates(List<ProductEstimate> uberProductEstimates)
        {
            //Clear the controls in the Documents Section
            divProductEstimates.Controls.Clear();

            HtmlGenericControl ul = new HtmlGenericControl("ul");
            ul.Attributes.Add("class", "list-group");

            int productEstimateCount = 0;
            foreach (var uberProductEstimate in uberProductEstimates)
            {
                if (uberProductEstimate.EstimateInfo.NoCarsAvailable == null)
                {
                    productEstimateCount++;
                    string idRadioButton = "RadioButton" + productEstimateCount.ToString();

                    //Add new Document Subcategory Header and start a new List
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    li.Attributes.Add("class", "list-group-item d-flex");

                    HtmlGenericControl divUberProductSelect = new HtmlGenericControl("div");
                    divUberProductSelect.Attributes.Add("class", "mr-4");
                    HtmlInputRadioButton radioButton = new HtmlInputRadioButton();
                    radioButton.Attributes.Add("type", "radio");
                    radioButton.Attributes.Add("id", idRadioButton);
                    radioButton.Attributes.Add("name", "RadioButtonList");
                    radioButton.Attributes.Add("value", uberProductEstimate.Product.ProductId);
                    radioButton.Attributes.Add("key", uberProductEstimate.Product.ProductId);
                    divUberProductSelect.Controls.Add(radioButton);


                    HtmlGenericControl divUberProduct = new HtmlGenericControl("div");
                    divUberProduct.InnerHtml = string.Format(@"<h5>&nbsp;{0}&nbsp;&nbsp;<small>{1}</small></h5>", uberProductEstimate.Product.DisplayName, uberProductEstimate.Product.Description) +
                                               string.Format(@"<h5>&nbsp;<strong>{0}</strong></h5>", uberProductEstimate.EstimateInfo.Fare.Display) +
                                              string.Format(@"<h5>&nbsp;Pickup in {0} minutes</h5>", uberProductEstimate.EstimateInfo.PickupEstimate.ToString());
                    divUberProduct.Attributes.Add("for", idRadioButton);


                    HtmlGenericControl divUberImage = new HtmlGenericControl("div");
                    divUberImage.Attributes.Add("class", "mr-4");
                    divUberImage.Style.Add("max-width", "90px");
                    divUberImage.Style.Add("background-image", "url('" + uberProductEstimate.Product.BackgroundImage + "')");
                    HtmlGenericControl uberImage = new HtmlGenericControl("img");
                    uberImage.Attributes.Add("class", "img-fluid");
                    uberImage.Attributes.Add("src", uberProductEstimate.Product.Image);
                    uberImage.Attributes.Add("for", idRadioButton);

                    divUberImage.Controls.Add(uberImage);


                    li.Controls.Add(divUberProductSelect);

                    li.Controls.Add(divUberImage);
                    li.Controls.Add(divUberProduct);


                    ul.Controls.Add(li);
                }

            }


            divProductEstimates.Controls.Add(ul);
        }



        #endregion




    }






}