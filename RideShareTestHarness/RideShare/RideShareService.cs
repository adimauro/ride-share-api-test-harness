﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RideShareTestHarness
{
    public class RideShareService : IRideShareService
    {
        public IRideShareService _rideShareService;

        public RideShareService(IRideShareService rideShareService)
        {
            this._rideShareService = rideShareService;
        }


        public string GetAuthorizationCode()
        {
            return _rideShareService.GetAuthorizationCode();
        }

        public string GetAccessToken(string authenticationCode)
        {
            return _rideShareService.GetAccessToken(authenticationCode);
        }

        public string GetProducts(string accessToken)
        {
            return _rideShareService.GetProducts(accessToken);
        }

        


    }
}