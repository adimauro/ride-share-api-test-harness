﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Net.Http;
using System.Text;
using System.IO;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;



namespace RideShareTestHarness
{
    public class LyftRideShareService : IRideShareService
    {
        private const string CLIENT_ID = "5rad-MjrSed7";
        private const string CLIENT_SECRET = "";


        public string ClientId { get; set; }
        public string ClientSecret { get; set; }


        public LyftRideShareService()
        {
            ClientId = CLIENT_ID;
            ClientSecret = CLIENT_SECRET;
        }

        public LyftRideShareService(string clientId, string clientSecret)
        {
            ClientId = clientId;
            ClientSecret = clientSecret;
        }



        public string GetAuthorizationCode()
        {

            throw new NotImplementedException();
        }


        public string GetAccessToken(string authenticationCode)
        {

            string accessTokenURI = @"https://api.lyft.com/oauth/token";


            var byteAuthorization = Encoding.ASCII.GetBytes(string.Format($"{ClientId}:{ClientSecret}"));


            var httpWebRequest = (HttpWebRequest)WebRequest.Create(accessTokenURI);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers["Authorization"] = "Basic " + Convert.ToBase64String(byteAuthorization);



            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"grant_type\":\"client_credentials\"," +
                              "\"scope\":\"public\"}";

                streamWriter.Write(json);
            }



            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                return streamReader.ReadToEnd();
            }
        }



        public string GetProducts(string accessToken)
        {

            throw new NotImplementedException();
        }


    }
}