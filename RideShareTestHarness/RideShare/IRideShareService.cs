﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideShareTestHarness
{
    public interface IRideShareService
    {

        string GetAuthorizationCode();

        string GetAccessToken(string authenticationCode);

        string GetProducts(string accessToken);

    }
}
