﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Net.Http;
using System.Text;
using System.IO;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace RideShareTestHarness
{
    public class UberRideShareService : IRideShareService
    {
        private const string CLIENT_ID = "5KXF6BwMtpmnbjeWN78_ZrCNWG4vsIIf";
        private const string CLIENT_SECRET = "7C-1ZaKyxeu_mkeY8haKqmUyuFyHlz_OswTaMbTU";
 

        private readonly string _version = "v2";
        private readonly string _clientId;
        private readonly string _clientSecret;


        public UberRideShareService()
        {
            this._clientId = CLIENT_ID;
            this._clientSecret = CLIENT_SECRET;
        }

        public UberRideShareService(string clientId, string clientSecret)
        {
            if (string.IsNullOrWhiteSpace(clientId)) throw new ArgumentException("Parameter is required", nameof(clientId));
            if (string.IsNullOrWhiteSpace(clientSecret)) throw new ArgumentException("Parameter is required", nameof(clientSecret));

            this._clientId = clientId;
            this._clientSecret = clientSecret;
        }




        public string GetAuthorizationCode()
        {
            string authorizationCodeURI = string.Format(@"https://login.uber.com/oauth/" + $"{this._version}/authorize");

            //var postParameters = new Dictionary<string, string>
            //{
            //    { "client_id", this._clientId },
            //    { "response_type", "code" },
            //    { "redirect_uri", "https://dev32qa.lightbeamhealth.com" }
            //};

            //var postParameters = new Dictionary<string, string>
            //{
            //    { "client_id", this._clientId },
            //    { "client_secret", this._clientSecret },
            //    { "response_type", "code" },
            //    { "redirect_uri", "https://dev32qa.lightbeamhealth.com" }
            //};



            var postParameters = new Dictionary<string, string>
            {
                { "client_id", this._clientId },
                { "client_secret", this._clientSecret },
                { "response_type", "code" },
            };

            string httpPostResponse = HttpPostRequest2(authorizationCodeURI, postParameters);
            //string httpPostResponse = HttpPostRequest(authorizationCodeURI, postParameters);

            /*
            var handler = new HttpClientHandler()
            {
                AllowAutoRedirect = false
            };
            HttpClient ApiHttpClient = new HttpClient(handler);



            ApiHttpClient.BaseAddress = new Uri($"{authorizationCodeURI.TrimEnd('/')}/");


            string postData = "";
            foreach (string key in postParameters.Keys)
            {
                postData += string.IsNullOrEmpty(postData) ? string.Empty : "&";
                postData += HttpUtility.UrlEncode(key) + "=" + HttpUtility.UrlEncode(postParameters[key]);
            }

            var requestUri = HttpUtility.UrlEncode(postData);


            var clientResponse = ApiHttpClient.GetAsync(requestUri).Result;


            var responseContent = clientResponse.Content.ReadAsStringAsync().Result;



            //string httpPostResponse = HttpPostRequest3(authorizationCodeURI, postParameters);
            */

            return "";
        }


        public string GetAccessToken(string authenticationCode)
        {

            string accessTokenURI = string.Format(@"https://login.uber.com/oauth/" + $"{this._version}/token");

            var postParameters = new Dictionary<string, string>
            {
                { "client_id", this._clientId },
                { "client_secret", this._clientSecret },
                { "grant_type", "authorization_code" },
                { "code", authenticationCode },
                { "redirect_uri", "https://dev32qa.lightbeamhealth.com/rideshare" }
            };



            string httpPostResponse = HttpPostRequest(accessTokenURI, postParameters);

            return httpPostResponse;
        }


        public string GetProducts(string accessToken)
        {

            string productURI = @"https://api.uber.com/v1.2/products?latitude=37.7752315&longitude=-122.418075";


            var byteAccessToken = Encoding.ASCII.GetBytes(accessToken);


            var httpWebRequest = (HttpWebRequest)WebRequest.Create(productURI);
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add("Authorization", "Bearer " + accessToken);
            //httpWebRequest.Headers["Authorization"] = "Bearer " + Convert.ToBase64String(byteAccessToken);

            //"Bearer " + Convert.ToBase64String(byteAccessToken);
            //"Basic " + Convert.ToBase64String(byteAccessToken);

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            return httpResponse.ToString();
        }



            //private string HttpPostRequest(string url, Dictionary<string, string> postParameters)
            //{
            //    string postData = "";

            //    foreach (string key in postParameters.Keys)
            //    {
            //        postData += HttpUtility.UrlEncode(key) + "="
            //              + HttpUtility.UrlEncode(postParameters[key]) + "&";
            //    }

            //    HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            //    httpWebRequest.Method = "POST";

            //    byte[] data = Encoding.ASCII.GetBytes(postData);

            //    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            //    httpWebRequest.ContentLength = data.Length;

            //    Stream requestStream = httpWebRequest.GetRequestStream();
            //    requestStream.Write(data, 0, data.Length);
            //    requestStream.Close();

            //    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //    {
            //        return streamReader.ReadToEnd();
            //    }
            //}




            private string HttpPostRequest(string url, Dictionary<string, string> postParameters)
        {
            string postData = string.Empty;

            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            httpWebRequest.Method = "POST";
            //httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.ContentType = "application/json";


            //foreach (string key in postParameters.Keys)
            //{
            //    postData += HttpUtility.UrlEncode(key) + "="
            //          + HttpUtility.UrlEncode(postParameters[key]) + "&";
            //}


            foreach (string key in postParameters.Keys)
            {
                postData += string.IsNullOrEmpty(postData) ? "{" : ",";
                postData += "\"" + HttpUtility.UrlEncode(key) + "\":\"" + HttpUtility.UrlEncode(postParameters[key]) + "\"";
            }
            postData += string.IsNullOrEmpty(postData) ? string.Empty : "}";




            //byte[] bytePostData = Encoding.ASCII.GetBytes(postData);
            //httpWebRequest.ContentLength = bytePostData.Length;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(postData);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                return streamReader.ReadToEnd();
            }
        }




        private string HttpPostRequest2(string url, Dictionary<string, string> postParameters)
        {

            string retval = "";
            //var httpWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("https://api.domo.com/oauth/token?grant_type=client_credentials&scope=data");
            var httpWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);            
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("cache-control", "no-cache");
            httpWebRequest.ContentType = "application/json";

            using (var streamWriter = new System.IO.StreamWriter(httpWebRequest.GetRequestStream()))
            {
                //string json = "{\"0c60a3f3-cb26-4c29-b95f-62c58e4e87f4\":\"c84099d3b6b72b0c124858dd28032bd446c3882e631d0cfc7471594c20cf15ad\"}";
                string json = "{";
                foreach (string key in postParameters.Keys)
                {
                    json += string.IsNullOrEmpty(json) ? string.Empty : ",";
                    json += string.Format("\"{0}\":\"{1}\"", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(postParameters[key]));
                }
                json += "}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (System.Net.HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new System.IO.StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    retval = result.ToString();
                }
            }
            return retval;


        }





        public async Task<string> GetAuthorizationCodeAsync()
        {
            string authorizationCodeURI = string.Format(@"https://login.uber.com/oauth/" + $"{this._version}/authorize");

            var postParameters = new Dictionary<string, string>
            {
                { "client_id", this._clientId },
                { "client_secret", this._clientSecret },
                { "response_type", "code" },
            };

            string postData = "";
            foreach (string key in postParameters.Keys)
            {
                postData += string.IsNullOrEmpty(postData) ? string.Empty : "&";
                postData += HttpUtility.UrlEncode(key) + "=" + HttpUtility.UrlEncode(postParameters[key]);
            }


            var url = authorizationCodeURI + "?" + postData;
            var response = await CoreTools.GetRedirectedUrl(url);





            return "";
        }




    }



    public static class CoreTools
        {
            public static async Task<string> GetRedirectedUrl(string url)
        {
            //this allows you to set the settings so that we can get the redirect url
            var handler = new HttpClientHandler()
            {

                AllowAutoRedirect = false
            };
            string redirectedUrl = null;

            using (HttpClient client = new HttpClient(handler))
            using (HttpResponseMessage response = await client.GetAsync(url))
            using (HttpContent content = response.Content)
            {
                // ... Read the response to see if we have the redirected url
                if (response.StatusCode == System.Net.HttpStatusCode.Found)
                {
                    HttpResponseHeaders headers = response.Headers;
                    if (headers != null && headers.Location != null)
                    {
                        redirectedUrl = headers.Location.AbsoluteUri;
                    }
                }
            }

            return redirectedUrl;
        }
    }
}