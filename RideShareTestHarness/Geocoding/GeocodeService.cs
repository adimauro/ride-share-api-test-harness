﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RideShareTestHarness
{
    public class GeocodeService : IGeocodeService
    {
        public IGeocodeService geocodeService;

        public GeocodeService(IGeocodeService geocodeService)
        {
            this.geocodeService = geocodeService;
        }


        public GeocoordinatesRecord GetCoordinates(string address, string city, string state, string zipCode)
        {
            return geocodeService.GetCoordinates(address, city, state, zipCode);
        }


        public LocationRecord GetCoordinates(LocationRecord location)
        {
            return geocodeService.GetCoordinates(location);
        }



        public LocationRecord GetAddress(double latitude, double longitude)
        {
            return geocodeService.GetAddress(latitude, longitude);
        }

        public LocationRecord GetAddress(GeocoordinatesRecord geocoordinates)
        {
            return geocodeService.GetAddress(geocoordinates);
        }



        public string GetZipCodeForAddress(string address, string city, string state)
        {
            return geocodeService.GetZipCodeForAddress(address, city, state);
        }


        public string GetZipCodeForAddress(LocationRecord location)
        {
            return geocodeService.GetZipCodeForAddress(location);

        }
    }
}