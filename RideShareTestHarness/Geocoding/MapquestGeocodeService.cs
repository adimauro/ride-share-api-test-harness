﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Net.Http;
using System.Text;
using System.IO;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RideShareTestHarness
{
    public class MapquestGeocodeService : IGeocodeService
    {
        private const string API_FORWARD_GEOGCODE_URI = @"http://open.mapquestapi.com/geocoding/v1/address";
        private const string API_REVESERE_GEOGCODE_URI = @"http://open.mapquestapi.com/geocoding/v1/reverse";
        private const string API_KEY = "P8NnDuaB6TZNakL3YFt19mvtiWvdQXLd";

        public MapquestGeocodeService()
        {
        }


        public LocationRecord GetCoordinates(LocationRecord locationRecord)
        {
            LocationRecord newlocationRecord = new LocationRecord();
            var geocoordinates = GetCoordinates(locationRecord.AddressLine1, locationRecord.City, locationRecord.State, locationRecord.ZipCode);
            if (geocoordinates != null)
            {
                newlocationRecord.Latitude = geocoordinates.Latitude;
                newlocationRecord.Longitude = geocoordinates.Longitude;

            }
            return newlocationRecord;
        }

        public GeocoordinatesRecord GetCoordinates(string address, string city, string state, string zipCode)
        {
            GeocoordinatesRecord geocoordinates = null;

            //SAMPLE URI
            //http://open.mapquestapi.com/geocoding/v1/address?key=KEY&street=1600+Pennsylvania+Ave+NW&city=Washington&state=DC&postalCode=20500

            string uriFormatedAddress = string.Format($"street={address}&city={city}&state={state}&postalCode={zipCode}");
            uriFormatedAddress = uriFormatedAddress.Replace(" ", "+");

            string geocodeUri = API_FORWARD_GEOGCODE_URI + "?key=" + API_KEY + "&" + uriFormatedAddress;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(geocodeUri);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.ContentLength = 0;
            httpWebRequest.Method = "POST";

            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
            {
                string webResponse = streamReader.ReadToEnd();
                
                var jsonWebResponse = JObject.Parse(webResponse);

                if (Convert.ToInt32(jsonWebResponse["info"]["statuscode"]) != 0 )
                {
                    //TO DO ERROR HANDLING
                }
                else
                {
                    geocoordinates = new GeocoordinatesRecord();
                    geocoordinates.Latitude = Convert.ToDouble(jsonWebResponse["results"][0]["locations"][0]["latLng"]["lat"]);
                    geocoordinates.Longitude = Convert.ToDouble(jsonWebResponse["results"][0]["locations"][0]["latLng"]["lng"]);
                }
            }

            return geocoordinates;
        }



        public LocationRecord GetAddress(GeocoordinatesRecord geocoordinates)
        {
            return GetAddress(geocoordinates.Latitude, geocoordinates.Longitude);
        }

        public LocationRecord GetAddress(double latitude, double longitude)
        {
            var locationRecord = new LocationRecord();

            locationRecord.Latitude = latitude;
            locationRecord.Longitude = longitude;

            //SAMPLE URI
            //http://open.mapquestapi.com/geocoding/v1/reverse?key=KEY&location=30.333472,-81.470448&includeRoadMetadata=true&includeNearestIntersection=true

            string uriFormatedCoordinates = string.Format($"location={latitude},{longitude}&includeRoadMetadata=true&includeNearestIntersection=true");
            uriFormatedCoordinates = uriFormatedCoordinates.Replace(" ", "+");

            string geocodeUri = API_REVESERE_GEOGCODE_URI + "?key=" + API_KEY + "&" + uriFormatedCoordinates;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(geocodeUri);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.ContentLength = 0;
            httpWebRequest.Method = "POST";

            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
            {
                string webResponse = streamReader.ReadToEnd();
                var jsonWebResponse = JObject.Parse(webResponse);

                if (Convert.ToInt32(jsonWebResponse["info"]["statuscode"]) != 0)
                {
                    //TO DO ERROR HANDLING
                }
                else
                {
                    locationRecord.AddressLine1 = jsonWebResponse["results"][0]["locations"][0]["street"].ToString();
                    locationRecord.AddressLine2 = jsonWebResponse["results"][0]["locations"][0]["adminArea6"].ToString();
                    locationRecord.City = jsonWebResponse["results"][0]["locations"][0]["adminArea5"].ToString();
                    locationRecord.State = jsonWebResponse["results"][0]["locations"][0]["adminArea3"].ToString();
                    locationRecord.ZipCode = jsonWebResponse["results"][0]["locations"][0]["postalCode"].ToString();
                }
            }

            return locationRecord;
        }





        public string GetZipCodeForAddress(LocationRecord locationRecord)
        {
            return GetZipCodeForAddress(locationRecord.AddressLine1, locationRecord.City, locationRecord.State);
        }


        public string GetZipCodeForAddress(string address, string city, string state)
        {
            string zipCode = string.Empty;

            //Ensure Street, City and State have been supplied
            if (!string.IsNullOrEmpty(address) && !string.IsNullOrEmpty(city) && !string.IsNullOrEmpty(state))
            {

                //SAMPLE URI
                //http://open.mapquestapi.com/geocoding/v1/address?key=KEY&street=1600+Pennsylvania+Ave+NW&city=Washington&state=DC&postalCode=20500

                string uriFormatedAddress = string.Format($"street={address}&city={city}&state={state}");
                uriFormatedAddress = uriFormatedAddress.Replace(" ", "+");

                string geocodeUri = API_FORWARD_GEOGCODE_URI + "?key=" + API_KEY + "&" + uriFormatedAddress;

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(geocodeUri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.ContentLength = 0;
                httpWebRequest.Method = "POST";

                var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    string webResponse = streamReader.ReadToEnd();

                    var jsonWebResponse = JObject.Parse(webResponse);

                    if (Convert.ToInt32(jsonWebResponse["info"]["statuscode"]) != 0)
                    {
                        //TO DO ERROR HANDLING
                    }
                    else
                    {
                        zipCode = jsonWebResponse["results"][0]["locations"][0]["postalCode"].ToString();
                    }
                }
            }

            return zipCode;
        }
    }
}