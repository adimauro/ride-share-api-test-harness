﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideShareTestHarness
{
    public interface IGeocodeService
    {
        GeocoordinatesRecord GetCoordinates(string address, string city, string state, string zipCode);

        LocationRecord GetCoordinates(LocationRecord locationRecord);


        LocationRecord GetAddress(double latitude, double longitude);

        LocationRecord GetAddress(GeocoordinatesRecord geocoordinates);


        string GetZipCodeForAddress(string address, string city, string state);

        string GetZipCodeForAddress(LocationRecord locationRecord);
    }
}
