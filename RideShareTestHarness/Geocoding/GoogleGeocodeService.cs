﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net;
using System.Net.Http;
using System.Text;
using System.IO;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;



namespace RideShareTestHarness
{
    public class GoogleGeocodeService : IGeocodeService
    {
        private const string API_FORWARD_GEOGCODE_URI = @"https://maps.googleapis.com/maps/api/geocode";
        private const string API_REVESERE_GEOGCODE_URI = @"https://maps.googleapis.com/maps/api/geocode";
        private const string API_KEY = "AIzaSyA64XcHR0O8RNLh1G-MvCicPRfGBktjOf4";


        public GoogleGeocodeService()
        {
        }


        public LocationRecord GetCoordinates(LocationRecord locationRecord)
        {
            LocationRecord newlocationRecord = new LocationRecord();
            var geocoordinates = GetCoordinates(locationRecord.AddressLine1, locationRecord.City, locationRecord.State, locationRecord.ZipCode);
            if (geocoordinates != null)
            {
                newlocationRecord.Latitude = geocoordinates.Latitude;
                newlocationRecord.Longitude = geocoordinates.Longitude;

            }
            return newlocationRecord;
        }


        public GeocoordinatesRecord GetCoordinates(string address, string city, string state, string zipCode)
        {
            GeocoordinatesRecord geocoordinates = null;

            //SAMPLE URI
            //https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY

            string uriFormatedAddress = address + " " + city + " " + state + " " + zipCode;
            uriFormatedAddress = HttpUtility.UrlEncode(uriFormatedAddress);

            string geocodeUri = API_FORWARD_GEOGCODE_URI + "/json?address=" + uriFormatedAddress + "&key=" + API_KEY;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(geocodeUri);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.ContentLength = 0;
            httpWebRequest.Method = "POST";

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                string webResponse = streamReader.ReadToEnd();
                var jsonWebResponse = JObject.Parse(webResponse);

                if (jsonWebResponse["status"].ToString() != "OK")
                {
                    //TO DO ERROR HANDLING
                }
                else
                {
                    geocoordinates = new GeocoordinatesRecord();
                    geocoordinates.Latitude = Convert.ToDouble(jsonWebResponse["results"][0]["geometry"]["location"]["lat"]);
                    geocoordinates.Longitude = Convert.ToDouble(jsonWebResponse["results"][0]["geometry"]["location"]["lng"]);
                }
            }

            return geocoordinates;
        }




        public LocationRecord GetAddress(GeocoordinatesRecord geocoordinates)
        {
            return GetAddress(geocoordinates.Latitude, geocoordinates.Longitude);
        }


        public LocationRecord GetAddress(double latitude, double longitude)
        {
            var locationRecord = new LocationRecord();

            locationRecord.Latitude = latitude;
            locationRecord.Longitude = longitude;

            //SAMPLE URI
            //https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY

            string uriFormatedCoordinates = string.Format($"latln={latitude},{longitude}");
            uriFormatedCoordinates = HttpUtility.UrlEncode(uriFormatedCoordinates);

            string geocodeUri = API_REVESERE_GEOGCODE_URI + "?key=" + API_KEY + "&" + uriFormatedCoordinates;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(geocodeUri);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.ContentLength = 0;
            httpWebRequest.Method = "POST";

            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
            {
                string webResponse = streamReader.ReadToEnd();
                var jsonWebResponse = JObject.Parse(webResponse);

                if (jsonWebResponse["status"].ToString() != "OK")
                {
                    //TO DO ERROR HANDLING
                }
                else
                {
                    string streetNumber = string.Empty;
                    string route = string.Empty;
                    string unit = string.Empty;
                    string city = string.Empty;
                    string state = string.Empty;
                    string zipCode = string.Empty;

                    var addressComponents = jsonWebResponse["results"][0]["address_components"];
                    foreach (var addressComponent in addressComponents)
                    {
                        if (addressComponent["types"][0].ToString() == "street_number")
                        {
                            streetNumber = addressComponent["long_name"].ToString();
                        }
                        if (addressComponent["types"][0].ToString() == "route")
                        {
                            route = addressComponent["long_name"].ToString();
                        }
                        if (addressComponent["types"][0].ToString() == "subpremise")
                        {
                            unit = addressComponent["long_name"].ToString();
                        }
                        if (addressComponent["types"][0].ToString() == "locality")
                        {
                            city = addressComponent["long_name"].ToString();
                        }
                        if (addressComponent["types"][0].ToString() == "administrative_area_level_1")
                        {
                            state = addressComponent["short_name"].ToString();
                        }
                        if (addressComponent["types"][0].ToString() == "postal_code")
                        {
                            zipCode = addressComponent["long_name"].ToString();
                        }
                    }

                    locationRecord.AddressLine1 = streetNumber + " " + route + (string.IsNullOrEmpty(unit) ? string.Empty : ", #" + unit);
                    locationRecord.City = city;
                    locationRecord.State = state;
                    locationRecord.ZipCode = zipCode;
                }
            }

            return locationRecord;
        }






        public string GetZipCodeForAddress(LocationRecord locationRecord)
        {
            return GetZipCodeForAddress(locationRecord.AddressLine1, locationRecord.City, locationRecord.State);
        }


        public string GetZipCodeForAddress(string address, string city, string state)
        {
            string zipCode = string.Empty;

            //Ensure Street, City and State have been supplied
            if (!string.IsNullOrEmpty(address) && !string.IsNullOrEmpty(city) && !string.IsNullOrEmpty(state))
            {
                //SAMPLE URI
                //https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY

                string uriFormatedAddress = address + " " + city + " " + state;
                uriFormatedAddress = HttpUtility.UrlEncode(uriFormatedAddress);

                string geocodeUri = API_FORWARD_GEOGCODE_URI + "/json?address=" + uriFormatedAddress + "&key=" + API_KEY;

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(geocodeUri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.ContentLength = 0;
                httpWebRequest.Method = "POST";

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string webResponse = streamReader.ReadToEnd();
                    var jsonWebResponse = JObject.Parse(webResponse);

                    if (jsonWebResponse["status"].ToString() != "OK")
                    {
                        //TO DO ERROR HANDLING
                    }
                    else
                    {
                        var addressComponents = jsonWebResponse["results"][0]["address_components"];
                        foreach (var addressComponent in addressComponents)
                        {
                            if (addressComponent["types"][0].ToString() == "postal_code")
                            {
                                zipCode = addressComponent["long_name"].ToString();
                            }
                        }
                    }
                }

            }

            return zipCode;
        }

    }
}