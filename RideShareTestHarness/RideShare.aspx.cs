﻿using System;
using Uber.Authentication.Data.Repository;
using Uber.Rider.Data.Repository;



namespace RideShareTestHarness
{
    public partial class RideShare : System.Web.UI.Page
    {
        private const string REDIRECT_URL = @"https://localhost:44360/rideshare.aspx";
        private const string CLIENT_ID = "5KXF6BwMtpmnbjeWN78_ZrCNWG4vsIIf";
        private const string CLIENT_SECRET = "7C-1ZaKyxeu_mkeY8haKqmUyuFyHlz_OswTaMbTU";


        protected void Page_Load(object sender, EventArgs e)
        {
            string activationCode = Request.QueryString["code"];
            AuthorizationCodeTextBox.Text = activationCode;
        }



        protected void GetAccessTokenButton_Click(object sender, EventArgs e)
        {
            GetAccessToken();
        }





        protected void GetUserProfileButton_Click(object sender, EventArgs e)
        {
            GetUserProfile();
        }

        

        protected void GetUserActivityButton_Click(object sender, EventArgs e)
        {

            GetUserActivity();
        }

        protected void GetProductsButton_Click(object sender, EventArgs e)
        {
            GetProducts();
        }




        private void GetAccessToken()
        {
            var uber = new UberAuthentication(CLIENT_ID, CLIENT_SECRET);


            var accessToken = uber.GetAccessToken(AuthorizationCodeTextBox.Text, REDIRECT_URL);
            AccessTokenTextBox.Text = accessToken.Value;

        }



        private void GetUserProfile()
        {
            var accessToken = AccessTokenTextBox.Text;

            var uber = new ClientAuthenticatedUberRiderService(accessToken);
            var userProfile = uber.GetUserProfile();


            if(userProfile.Error != null)
            {
                UserProfileTextBox.Text = "Code: " + (string.IsNullOrEmpty(userProfile.Error.Code) ? string.Empty : userProfile.Error.Code) + Environment.NewLine +
                                        "Message: " + (string.IsNullOrEmpty(userProfile.Error.Message) ? string.Empty : userProfile.Error.Message);
            }
            else
            {

            UserProfileTextBox.Text = "First Name: " + (string.IsNullOrEmpty(userProfile.Data.FirstName) ? string.Empty : userProfile.Data.FirstName) + Environment.NewLine +
                                  "Last Name: " + (string.IsNullOrEmpty(userProfile.Data.LastName) ? string.Empty : userProfile.Data.LastName)  + Environment.NewLine +
                                  "Rider Id: " + (string.IsNullOrEmpty(userProfile.Data.RiderId) ? string.Empty : userProfile.Data.RiderId) ;
            }
        }



        private void GetUserActivity()
        {
            var accessToken = AccessTokenTextBox.Text;

            var uber = new ClientAuthenticatedUberRiderService(accessToken);
            var userActivity = uber.GetUserActivity(0, 5);

            if (userActivity.Error != null)
            {
                UserActivityTextBox.Text = "Code: " + (string.IsNullOrEmpty(userActivity.Error.Code) ? string.Empty : userActivity.Error.Code) + Environment.NewLine +
                                        "Message: " + (string.IsNullOrEmpty(userActivity.Error.Message) ? string.Empty : userActivity.Error.Message);
            }
            else
            {
                int rideNumber = 0;
                string userActivityData = string.Empty;
                foreach(var userHistory in userActivity.Data.History)
                {
                    rideNumber++;
                    userActivityData = "Ride Number: " + rideNumber.ToString() + Environment.NewLine +
                                       "Request Time: " + (string.IsNullOrEmpty(userHistory.RequestTime.ToString()) ? string.Empty : userHistory.RequestTime.ToString()) + Environment.NewLine +
                                       "Start City: " + (string.IsNullOrEmpty(userHistory.RequestLocation.DisplayName) ? string.Empty : userHistory.RequestLocation.DisplayName) + Environment.NewLine +
                                       "Location: " + (string.IsNullOrEmpty(userHistory.RequestLocation.Latitude.ToString()) ? string.Empty : userHistory.RequestLocation.Latitude.ToString()) + ", "
                                                    + (string.IsNullOrEmpty(userHistory.RequestLocation.Latitude.ToString()) ? string.Empty : userHistory.RequestLocation.Latitude.ToString()) +
                                       "Start Time: " + (string.IsNullOrEmpty(userHistory.StartTime.ToString()) ? string.Empty : userHistory.StartTime.ToString()) + Environment.NewLine +
                                       "End Time: " + (string.IsNullOrEmpty(userHistory.EndTime.ToString()) ? string.Empty : userHistory.EndTime.ToString()) + Environment.NewLine +
                                       "Distance: " + (string.IsNullOrEmpty(userHistory.Distance.ToString()) ? string.Empty : userHistory.Distance.ToString()) + Environment.NewLine +
                                       "Status: " + (string.IsNullOrEmpty(userHistory.Status) ? string.Empty : userHistory.Status) + Environment.NewLine +
                                       Environment.NewLine;
                }

                UserActivityTextBox.Text = userActivityData;
                
            }
        }



        private void GetProducts()
        {
            var accessToken = AccessTokenTextBox.Text;

            var uber = new ClientAuthenticatedUberRiderService(accessToken);

            //Lat & Lng for: 400 Norristown Rd, Horsham, PA 19044
            double latitude = 40.183998;
            double longitude = -75.1373936;

            var products = uber.GetProducts(latitude, longitude);

            if (products.Error != null)
            {
                ProductsTextBox.Text = "Code: " + (string.IsNullOrEmpty(products.Error.Code) ? string.Empty : products.Error.Code) + Environment.NewLine +
                                        "Message: " + (string.IsNullOrEmpty(products.Error.Message) ? string.Empty : products.Error.Message);
            }
            else
            {
                int productNumber = 0;
                string productData = string.Empty;
                foreach (var product in products.Data.Products)
                {
                    productNumber++;
                    productData = "Product Number: " + productNumber.ToString() + Environment.NewLine +

                                       "Product: " + (string.IsNullOrEmpty(product.DisplayName) ? string.Empty : product.DisplayName) + Environment.NewLine +
                                       "Capacity: " + (string.IsNullOrEmpty(product.Capacity.ToString()) ? string.Empty : product.Capacity.ToString()) + Environment.NewLine +
                                       "Image: " + (string.IsNullOrEmpty(product.Image) ? string.Empty : product.Image) + Environment.NewLine +

                                       "Base Cost: " + (string.IsNullOrEmpty(product.PriceDetails.Base.ToString()) ? string.Empty : product.PriceDetails.Base.ToString()) + Environment.NewLine +
                                       "Min Cost: " + (string.IsNullOrEmpty(product.PriceDetails.Minimum.ToString()) ? string.Empty : product.PriceDetails.Minimum.ToString()) + Environment.NewLine +
                                       "Cost per Minute: " + (string.IsNullOrEmpty(product.PriceDetails.CostPerMinute.ToString()) ? string.Empty : product.PriceDetails.CostPerMinute.ToString()) + Environment.NewLine +
                                       "Cost per Distance: " + (string.IsNullOrEmpty(product.PriceDetails.CostPerDistance.ToString()) ? string.Empty : product.PriceDetails.CostPerDistance.ToString()) + Environment.NewLine +
                                       Environment.NewLine;
                }

                ProductsTextBox.Text = productData;
            }
        }
    }






}