﻿<%@ Page Language="C#" AutoEventWireup="true"  Async="true" MaintainScrollPositionOnPostback="true"  CodeBehind="UberHealth.aspx.cs" Inherits="RideShareTestHarness.UberHealth"   %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LB Ride Integration Test App</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">


    </style>
</head>

<script>

    function GetSelectedProduct() {
        var hiddenProductField = document.getElementById("<%= SelectedProductHiddenField.ClientID %>");
        hiddenProductField.value = "";

        //Get the Checkbox List
        var radioButtons = document.getElementById("divProductEstimates").getElementsByTagName("input");

        //Loop through the Radio Button list to detemine which is selected
        for (var i = 0; i < radioButtons.length; i++) {
            if (radioButtons[i].type == "radio") {
                if (radioButtons[i].checked) {
                    alert(radioButtons[i].value);
                    //CheckBox is checked, add checkbox value to the list
                    hiddenProductField.value = radioButtons[i].value;
                }
            }
        }
    }

</script>
<body>
<div class="container-fluid">

    <form id="form1" runat="server">
        <div class="pl-5 pr-5">
            <br />
            <h2 style="box-sizing: border-box; font-variant-ligatures: normal; font-variant-caps: normal; orphans: 2; widows: 2; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">Authentication</h2>
            <div class="form-row">
                <div class="col-md-12">
                    <asp:Label ID="AccessTokenLabel" runat="server" Text="Access Token"></asp:Label>
                    <asp:TextBox ID="AccessTokenTextBox" runat="server" class="form-control"></asp:TextBox>
		        </div>
		    </div>

		    <div class="form-row pt-2">
                <div class="col-md-4">
                    <asp:Label ID="SandboxRideTypeLabel" runat="server" Text="Ride Type"></asp:Label>
	                <asp:DropDownList ID="SandboxRideTypeDropDown" runat="server"  class="form-control" >

                    </asp:DropDownList>
                    <asp:Button ID="SandboxRunButton" runat="server" class="btn btn-secondary" Text="Create Sandbox Run" OnClick="CreateSandboxRunButton_Click" />
                </div>
                <div class="col-md-4">
                    <asp:Label ID="SandboxRunIdLabel" runat="server" Text="Sandbox Run Id"></asp:Label>
                    <asp:TextBox ID="SandboxRunIdTextBox" runat="server" class="form-control"></asp:TextBox>
		        </div>
                <div class="col-md-4">
                    <asp:Label ID="SandboxDriverLabel" runat="server" Text="Sandbox Driver"></asp:Label>
                    <asp:TextBox ID="SandboxDriverTextBox" runat="server" class="form-control"></asp:TextBox>
                    <asp:Button ID="SandboxDriverButton" runat="server" class="btn btn-secondary" Text="Get Sandbox Driver" OnClick="GetSandboxDriverButton_Click" />
		        </div>
		    </div>

            <br /><br />

            <h2 style="box-sizing: border-box; font-variant-ligatures: normal; font-variant-caps: normal; orphans: 2; widows: 2; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">Trip Estimates</h2>
            <div class="form-row">
                <div class="col-md-6">
                    <asp:Button ID="GetProductsButton" runat="server" class="btn btn-secondary" Text="Get Trip Estimates" OnClick="GetProductsButton_Click"  />
                    <br /> <br />
                    <div id="divProductEstimates" runat="server">

                    </div>
                    <asp:HiddenField ID="SelectedProductHiddenField" runat="server" />
                    <div id="divTripRequestButton" class="mt-4">
                        <asp:Button ID="TripRequestButton" runat="server" class="btn btn-secondary" Text="Request Trip" OnClientClick="return GetSelectedProduct()" OnClick="TripRequestButton_Click"  />
                    </div>
                </div>
            </div>

            <br />


            <h2 style="box-sizing: border-box; font-variant-ligatures: normal; font-variant-caps: normal; orphans: 2; widows: 2; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">Trip Request</h2>
            <div class="form-row">
                <div class="col-md-6">
                    <asp:Label ID="TripResponseLabel" runat="server" Text="Trip Response"></asp:Label>
                    <asp:TextBox ID="TripResponseTextBox" runat="server" ReadOnly="True" Rows="4" TextMode="MultiLine" class="form-control"></asp:TextBox>
                    <br />
                    <asp:Label ID="SandboxDriverStateLabel" runat="server" Text="Driver State"></asp:Label>
                    
                    

	                <asp:DropDownList ID="SandboxDriverStateDropDown" runat="server"  class="form-control" >
                    </asp:DropDownList>
                    <asp:Button ID="SetSandboxDriverStateButton" runat="server" class="btn btn-secondary" Text="Set Driver State" OnClick="SetSandboxDriverStateButton_Click" />

                </div>
            </div>



        </div>
    </form>
</div>
</body>
</html>
